# Documentação


## Introdução
Scala e uma linguagem semelhante a java com a possibilidade de uso de biblioteca da mesma.

Scala não possui historia, ela começou apos o fracasso de um linguagem criada pelo próprio criador de Scala que decidiu criar uma linguagem mais fácil orientada a objetos e programação funcional.



## Historia

Scala e uma linguagem de programação criada de proposito geral.

Foi criado por Martin Odersky, no ano de 2001 Ecole Polytechnique Federale de Lausanne (EPFL), porem só foi lançada oficialmente em 2004.

Scala não possui historia, apenas surgiu com o fracasso de uma linguagem criada pelo Martin como conta em seu blog.

A Linguagem scala apenas de ser traduzida para bytecode e JVM, não e uma extensão de java, porem bibliotecas de java podem ser usadas nela.

A linguagem foi produzida com os paradigmas de orientação a objetos e programação funcional.

A linguagem e puramente orientada a objetos pois cada valor e um objeto e funcional pois toda função e um valor. 

# Características técnicas
### Instalação
*Referencia: ubuntu*
- Para usar a linguagem e necessário ter JAVA JDK instalado na maquina
- Para instala a linguage, no terminal digite:
```
sudo apt-get install scala 
```

- Para compilar um arquivo scala, no terminal digite:
```
scala file.scala
```
Substituindo "file" pelo nome do arquivo a ser compilado.
  
  

### Principais características
- Inferência de Tipos: Não precisamos declarar explicitamente o tipo das variáveis, pois o próprio compilador infere o tipo.

- Pattern Matching: Trabalha semelhante ao Switch/Case, porem e possível fazer a comparação utilizando expressões regulares.

- Traits: Como uma interface Java, porem permite que métodos sejam implementados, e não apenas declarados.

- Funções de Alta Ordem: funções são objetos, por isso podem ser passadas como parâmetro para outras funções.

- Objeto: Em Scala tudo e objeto, ate mesmo variáveis.






### Paradigmas suportados
- Orientação a objetos, Programação funcional.

### Compilação
- A código em scala e convertido para
bytecode, posteriormente na JVM e
transformado em código de máquina.

### EBNF Da linguagem
- A EBNF da linguagem pode ser encontrada em: [EBNF](https://scala-lang.org/files/archive/spec/2.13/01-lexical-syntax.html)

### Variação de tipos
- Foi criada para expressar padrões de programação comuns de forma concisa, elegante e fortemente tipada.

### VAR VS VAL
- var pode ser mudado a qualquer momento pelo programadador.
- val só pode ser mudada na criação.


### Principais tipos de variáveis
- Boolean: true ou false;
- Short: 16 bits com sinal;
- Char: 16 bits sem sinal;
- Int: 32 bits com sinal;
- Long: 64 bits com sinal;
- Float: 32 bits IEEE precisão simples;
- Double: 64 bits IEEE precisão dupla;
- String: Sequência de caracteres.

### Principais IDE
- Pode ser implementada usando IntelliJ IDEA, VS Code, GNU Emacs, Vim, Sublime Text e Aton.



### IF/ELSE
- Em Scala é necessário o uso de chaves {} para especificar o início e fim de um if. O else so pode ser usado depois do uso de um if ou if seguido de else if.
```
if(x<10){

} else if(x>10){

} else{

}
```

### For

```
for( n <- 0 to 5) 
```
### For em lista
- n percorre toda lista e imprime seu conteúdo 
```
val nums = Seq(1,2,3);
for (n <- nums) println(n);

```
### Array Unidimensional 
```
var days = Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ) 
for ( m1 <-days ) println(m1) 
```

Acesso de posição de array: println(days(n)), onde "n" representa a posição a ser acessada. 



### Array Multidimensional (Matriz)
- Para usar um array multidimensional é necessário especificar na criação do array que ele e multidimensional usando “Array.ofDim”.
```
var Mat= Array.ofDim[Int](2, 2)
Mat(0)(0) = 2
Mat(0)(1) = 7
Mat(1)(0) = 3
Mat(1)(1) = 4
for(i<-0 to 1; j<-0 until 2)
  println(i, j + " = " + Mat(i)(j))

```

### Função
Toda funçao em scala começa com def seguido pelo nome.

Os paremetros sao passados entre parenteses bem como seu tipo.

Apos a passagem de parametros e fechamento dos parenteses seguido pelo : e inserido o tipo de retorno.   


```
def soma(x: Int, y: Int ): Int = {
     x + y
  }


```

### Função

passagem de função como parametro.

```
def sum(x:Int, y:Int) = x+y;
def dobro(x:Int) = x+x;
def main(args: Array[String]) {
  print(sum(dobro(10),7));
}


```


### Try/Catch

```
var text = “”
try {
  text = openAndReadAFile(filename)
} catch {
  case e: FileNotFoundException =>
  println("Couldn't find that file.")
  case e: IOException => println("Had an
  IOException trying to read that file")
}
```



## Referencias
[Conheça a Linguagem Scala](https://www.devmedia.com.br/conheca-a-linguagem-scala/32850)

[História](https://www.javatpoint.com/history-of-scala)

[Blog do Martin](https://www.artima.com/weblogs/viewpost.jsp?thread=163733)

[Tour pela linguagem](https://docs.scala-lang.org/tour/tour-of-scala.html)